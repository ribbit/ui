# Ribbit UI

This is the UI for Ribbit, written with React and Material-UI.

## Deployments

This static site is deployed using the AWS Amplify Console.

Dev: https://ribbit.andrewohara.dev

Prod: https://ribbit.andrewohara.prod

## Run Locally

The `REACT_APP_GOOGLE_CLIENT_ID` environment variable is required.  This can be done with dotenv.

```
# .env.local
REACT_APP_GOOGLE_CLIENT_ID=<GoogleClientId>
```

```shell
$ npm i
$ npm start
```