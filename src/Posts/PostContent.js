import React, { Component } from 'react'

import { withStyles } from '@material-ui/core/styles'

import { parseUrl } from 'query-string'

const styles = {
  youtubeContainer: {
    position: 'relative',
    'padding-bottom': '56.25%',
    'padding-top': 30,
    height: 0,
    overflow: 'hidden'
  },
  youtubeFrame: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%'
  }
}

class PostContent extends Component {
  render () {
    const { post, classes } = this.props
    if (!post.type || !post.value) {
      return (
        <div>No content</div>
      )
    }

    const { type, value } = post

    if (type === 'text') {
      return (
        <div>{value}</div>
      )
    } else if (type === 'image') {
      return (
        <div>
          <img src={value} alt={post.title} style={{ maxWidth: '100%' }} />
        </div>
      )
    } else if (type === 'youtube') {
      const url = parseUrl(value)
      return (
        <div className={classes.youtubeContainer}>
          <iframe
            className={classes.youtubeFrame}
            id={`post-${post.id}-content-youtube`}
            title={post.title}
            type='text/html'
            src={`https://www.youtube.com/embed/${url.query.v}`}
            frameBorder='0'
          />
        </div>
      )
    } else if (type === 'link') {
      return (
        <a href={value}>
          {value}
        </a>
      )
    } else if (type === 'video') {
      return (
        <video controls muted width='100%'>
          <source src={value} />
          Sorry, your browser doesn't support embedded videos.
        </video>
      )
    }

    return (
      <div>Cannot display {type} content</div>
    )
  }
}
export default withStyles(styles)(PostContent)
