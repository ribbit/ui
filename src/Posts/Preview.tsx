import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import Moment from 'react-moment'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardHeader from '@material-ui/core/CardHeader'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'

import PostContent from './PostContent'
import { Context } from '../Context'
import SubribbitAvatar from '../Subribbits/SubribbitAvatar'
import PostsClient, { Post } from '../api/postsClient'
import UsersClient from '../api/usersClient'


interface Props {
  post: Post
}

interface User {
  name: string
}

interface State {
  deleted: boolean
  author: User | null
}

export default class Preview extends Component<Props, State> {
  public state = { deleted: false, author: null }

  constructor (props: Props) {
    super(props)
    this.DeleteButton = this.DeleteButton.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
  }

  async componentDidMount () {
    const { post } = this.props
    const usersClient: UsersClient = this.context.usersClient
    const author = await usersClient.get(post.author)
    if (author !== null) {
      this.setState({ author })
    }
  }

  render () {
    const { deleted, author }: State = this.state
    const { post } = this.props
    const { user } = this.context

    const subribbitUrl = `/r/${post.subribbit}`

    if (deleted) {
      return <Redirect to={subribbitUrl} />
    }

    function authorName(): string {
      return (author) ? author.name : post.author
    }

    return (
      <Card>
        <CardHeader
          avatar={<SubribbitAvatar subribbit={post.subribbit} />}
          title={post.title}
          subheader={
            <span>
              to {<Link to={subribbitUrl}>{subribbitUrl}</Link>}
              &nbsp;by {authorName()} {<Moment date={post.posted} fromNow withTitle />}
            </span>
          }
          titleTypographyProps={{ variant: 'h5', component: 'h2' }}
        />
        <CardContent>
          <PostContent post={post} />
        </CardContent>
        <CardActions>
          {user.getId() === post.author ? <this.DeleteButton /> : null}
        </CardActions>
      </Card>
    )
  }

  async handleDelete () {
    const postsClient: PostsClient = this.context.postsClient
    const { post } = this.props

    await postsClient.delete(post)
    this.setState({ deleted: true })
  }

  DeleteButton () {
    return (
      <Button size='small' onClick={this.handleDelete}>Delete</Button>
    )
  }
}
Preview.contextType = Context
