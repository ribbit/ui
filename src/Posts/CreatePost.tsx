import React, { Component, ChangeEvent, MouseEvent } from 'react'
import { Link, Redirect } from 'react-router-dom'

import TextField from '@material-ui/core/TextField'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'

import { Context } from '../Context'
import PostsClient, { PostType, Post } from '../api/postsClient'

interface Props {
  subribbitName: string
}

interface State {
  title: string
  type: string
  value: string
  result: Post | null
}

export default class CreatePost extends Component<Props, State> {
  public state = { title: '', type: PostType.text, value: '', result: null }

  handleTitleChange (event: ChangeEvent<HTMLInputElement>) {
    this.setState({
      title: event.target.value
    })
  }

  handleTypeChange (event: ChangeEvent<{name?: string | undefined; value: unknown}>) {
    const type: string = event.target.value as string
    this.setState({ type })
  }

  handleValueChange (event: ChangeEvent<HTMLInputElement>) {
    this.setState({
      value: event.target.value
    })
  }

  async handleSubmit (event: MouseEvent<HTMLButtonElement>) {
    const postsClient: PostsClient = this.context.postsClient
    const { subribbitName } = this.props
    const { title, type, value } = this.state

    const postType : PostType = PostType[type]
    const response = await postsClient.create(subribbitName, title, postType, value)
    const result: Post = response.data
    this.setState({ result })
  }

  render () {
    const result: Post | null = this.state.result
    const { title, type, value } = this.state
    const { subribbitName } = this.props

    if (result !== null) {
      return <Redirect to={`/r/${result!.subribbit}`} />
    }

    return (
      <div>
        <h1>Create Post for /r/{subribbitName}</h1>

        <TextField
          autoFocus
          margin='dense'
          id='title'
          label='Title'
          fullWidth
          required
          onChange={this.handleTitleChange.bind(this)}
          value={title}
        />

        <Select value={type} onChange={this.handleTypeChange.bind(this)} >
          {Object.keys(PostType).map(type => {
            return <MenuItem key={type} value={type}>{type}</MenuItem>
          })}
        </Select>

        <TextField
          autoFocus
          margin='dense'
          id='value'
          label='Content'
          fullWidth
          required
          onChange={this.handleValueChange.bind(this)}
          value={value}
        />

        <Button to={`/r/${subribbitName}`} component={Link} color='secondary'>
          Cancel
        </Button>
        <Button onClick={this.handleSubmit.bind(this)} color='primary'>
          Create
        </Button>
      </div>
    )
  }
}
CreatePost.contextType = Context
