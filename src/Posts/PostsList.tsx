import React, { Component } from 'react'

import CircularProgress from '@material-ui/core/CircularProgress'
import Grid from '@material-ui/core/Grid'

import Preview from './Preview'
import { Context } from '../Context'
import SubribbitTools from '../Subribbits/SubribbitTools'
import PostsClient, { Post } from '../api/postsClient'
import { Subribbit } from '../api/subribbitsClient'

interface Props {
  subribbit: Subribbit
}

interface State {
  posts: Post[]
  loading: boolean
}

function Sidebar(props: { subribbit: Subribbit, userId: string }) {
  const { subribbit } = props
  return (
    <div>
      <SubribbitTools subribbit={subribbit} />
    </div>
  )
}

export default class PostsList extends Component<Props, State> {
  public state = { posts: [], loading: false }

  componentDidMount () {
    this.fetch()
  }

  componentDidUpdate (prevProps: Props) {
    if (this.props.subribbit !== prevProps.subribbit) {
      this.fetch()
    }
  }

  async fetch () {
    const { subribbit } = this.props
    const postsClient: PostsClient = this.context.postsClient

    this.setState({
      loading: true
    })

    const postsPromise = postsClient.list(subribbit.name)

    const posts: Post[] = (await postsPromise).data
    this.setState({ posts, loading: false })
  }

  render () {
    const { subribbit } = this.props
    const { loading, posts } = this.state
    const userId: string = this.context.user.getId()

    return (
      <Grid container spacing={4}>
        <Grid item xs={12} sm={2} md={4}>
          {subribbit ? <Sidebar subribbit={subribbit!} userId={userId} /> : null}
        </Grid>
        <Grid item xs={12} sm={8} md={4}>
          {
            loading
              ? <CircularProgress />
              : posts.map((post: Post) => <Preview key={post.id} post={post} />)
          }
        </Grid>
        <Grid item xs={12} sm={false} md={4} />
      </Grid>
    )
  }
}
PostsList.contextType = Context
