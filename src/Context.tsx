import React, { Component } from 'react'
import PostsClient from './api/postsClient'
import UsersClient from './api/usersClient'
import SubribbitsClient from './api/subribbitsClient'
import Auth from './api/auth'

export const Context = React.createContext({})

interface State {
  googleUser: any | null
  posts: PostsClient | null
  users: UsersClient | null
  subribbits: SubribbitsClient | null
}

export class Provider extends Component<{}, State> {
  public state = {
    googleUser: null,
    posts: null,
    users: null,
    subribbits: null
  }

  setUser (googleUser: any) {
    if (googleUser) {
      const auth = new Auth(googleUser)

      const users = new UsersClient(auth)
      users.onLogin(googleUser)     

      this.setState({
        googleUser,
        users,
        posts: new PostsClient(auth),
        subribbits: new SubribbitsClient(auth)
      })
    } else {
      this.setState({
        googleUser: null,
        users: null,
        posts: null,
        subribbits: null
      })
    }
  }

  render () {
    const { children } = this.props

    return (
      <Context.Provider
        value={{
          user: this.state.googleUser,
          loggedIn: this.state.googleUser !== null,
          setUser: this.setUser.bind(this),
          usersClient: this.state.users,
          postsClient: this.state.posts,
          subribbitsClient: this.state.subribbits
        }}
      >
        {children}
      </Context.Provider>
    )
  }
}
