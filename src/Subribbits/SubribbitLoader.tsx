import React, { Component } from 'react'

import CircularProgress from '@material-ui/core/CircularProgress'

import SubribbitsClient, { Subribbit, DEFAULT_SUBRIBBIT } from '../api/subribbitsClient'
import { Context } from '../Context'
import PostsList from '../Posts/PostsList'

interface Props {
  name: string
}

interface State {
  subribbit: Subribbit | null
  loading: boolean
}

export default class SubribbitLoader extends Component<Props, State> {
  state = { loading: false, subribbit: null }

  async componentDidMount () {
   this.load()
  }

  async load () {
    const { name } = this.props
    const client: SubribbitsClient = this.context.subribbitsClient

    if (name === DEFAULT_SUBRIBBIT.name) {
      this.setState({ subribbit: DEFAULT_SUBRIBBIT })
      return
    }
    
    this.setState({ loading: true })
    const subribbit = await client.getSubribbit(name)
    if (!subribbit) {
      throw Error(`Subribbit not found: ${name}`)
    }
    this.setState({ loading: false, subribbit })
  }

  componentDidUpdate (prevProps: Props) {
    if (this.props.name !== prevProps.name) {
      this.load()
    }
  }

  render () {
    const { subribbit } = this.state
    if (!subribbit) {
      return <CircularProgress />
    }
    return <PostsList subribbit={subribbit!} />
  }

}
SubribbitLoader.contextType = Context