import React, { Component } from 'react'
import { withRouter, RouteComponentProps } from 'react-router-dom'

import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import CircularProgress from '@material-ui/core/CircularProgress'

import SubribbitsClient, { Subribbit } from '../api/subribbitsClient'
import ConfirmButton from '../Widgets/ConfirmButton'
import { Context } from '../Context'

type Props = RouteComponentProps & {
  subribbit: Subribbit,
}

type State = {
  subribbit: Subribbit
  loading: boolean
}

class SubribbitAdminCard extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    const { subribbit } = props
    this.state = { subribbit, loading: false }
  }

  async handleMakePrivate() {
    const { subribbit } = this.state

    this.setState({ loading: true })
    const client: SubribbitsClient = this.context.subribbitsClient
    const updated = await client.makePrivate(subribbit.name)

    this.setState( { subribbit: updated, loading: false })
  }

  async handleMakePublic() {
    const { subribbit } = this.state

    this.setState({ loading: true })
    const client: SubribbitsClient = this.context.subribbitsClient
    const updated = await client.makePublic(subribbit.name)

    this.setState( { subribbit: updated, loading: false })
  }

  async handleDelete() {
    const { history, subribbit } = this.props
    const client: SubribbitsClient = this.context.subribbitsClient

    this.setState({ loading: true })
    await client.delete(subribbit.name)

    history.push('/')
  }

  render () {
    const { subribbit, loading } = this.state

    if (subribbit === null) {
      return <div />
    }

    const MakePrivateButton = () => (
      <ConfirmButton
        color='secondary'
        onClick={this.handleMakePrivate.bind(this)}
        disabled={loading}
      >
        Make Private
      </ConfirmButton>
    )

    const MakePublicButton = () => (
      <ConfirmButton
        color='primary'
        onClick={this.handleMakePublic.bind(this)}
        disabled={loading}
      >
        Make Public
      </ConfirmButton>
    )

    return (
      <Card>
        <CardHeader title='Configure Subribbit' />
        <CardContent>
          {loading ? <CircularProgress /> : null}
          {subribbit.private
            ? <div>
                <div><MakePublicButton /></div>
              </div>
            : <div><MakePrivateButton /></div>
            }
          <div>
            <ConfirmButton
              color='secondary'
              onClick={this.handleDelete.bind(this)}
              disabled={loading}
            >
              Delete
            </ConfirmButton>
          </div>
        </CardContent>
      </Card>
    )
  }
}
SubribbitAdminCard.contextType = Context
export default withRouter(SubribbitAdminCard)
