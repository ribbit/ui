import React, { Component, ChangeEvent } from 'react'
import { withRouter, RouteComponentProps } from 'react-router-dom'

import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import Typography from '@material-ui/core/Typography'

import { Context } from '../Context'
import SubribbitAvatar from './SubribbitAvatar'
import SubribbitsClient, { Subribbit, DEFAULT_SUBRIBBIT } from '../api/subribbitsClient'

type Props = RouteComponentProps & {
  selected: Subribbit,
}

interface State {
  open: boolean
  loading: boolean
  options: Subribbit[]
}

class SubribbitSelector extends Component<Props, State> {
  state = { open: false, loading: false, options: [] }

  async componentDidMount () {
    const subribbitsClient: SubribbitsClient = this.context.subribbitsClient
    const { selected } = this.props

    this.setState({ loading: true })

    const options = await subribbitsClient.getSubscriptions()

    if (!options.find(o => o.name === DEFAULT_SUBRIBBIT.name)) {
      options.push(DEFAULT_SUBRIBBIT)
    }
    if (!options.find(o => o.name === selected.name)) {
      options.push(selected)
    }

    this.setState({ options, loading: false })
  }

  handleOpen () {
    this.setState({ open: true })
  }

  handleClose () {
    this.setState({ open: false })
  }

  handleChange (event: ChangeEvent<{ name?: string | undefined; value: unknown; }>) {
    const { history } = this.props
    const selected: string = event.target.value as string
    history.push(`/r/${selected}`)
  }

  render () {
    const { selected } = this.props
    const { options, open } = this.state

    return (
      <Select
        open={open}
        onClose={this.handleClose.bind(this)}
        onOpen={this.handleOpen.bind(this)}
        onChange={this.handleChange.bind(this)}
        value={selected.name}
        style={{ width: '100%' }}
      >
        {options.map((subribbit: Subribbit) => {
          const name: string = subribbit.name
          const url = `/r/${name}`
          return (
            <MenuItem key={name} value={name}>
              <SubribbitAvatar subribbit={name} />
              <Typography color='inherit'>
                {url}
              </Typography>
            </MenuItem>
          )
        })}
      </Select>
    )
  }
}
SubribbitSelector.contextType = Context
export default withRouter(SubribbitSelector)
