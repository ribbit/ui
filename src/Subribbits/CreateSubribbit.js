import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'

import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import FormGroup from '@material-ui/core/FormGroup'
import Paper from '@material-ui/core/Paper'
import { withStyles } from '@material-ui/core/styles'

import { Context } from '../Context'

const styles = theme => ({
  root: {
    maxWidth: '100%',
    [theme.breakpoints.up('sm')]: {
      maxWidth: 720
    },
    margin: `${theme.spacing(1)}px auto`,
    padding: theme.spacing(3, 2)
  }
})

class CreateSubribbit extends Component {
  constructor (props) {
    super(props)

    this.state = {
      name: '',
      result: null,
      whitelist: null
    }

    this.handleNameChange = this.handleNameChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleNameChange (event) {
    this.setState({
      name: event.target.value
    })
  }

  async handleSubmit (event) {
    const { subribbitsClient } = this.context
    const { name } = this.state

    try {
      const result = await subribbitsClient.create(name)
      this.setState({ result })
    } catch (e) {
      window.alert(e)
    }
  }

  render () {
    const { name, result } = this.state
    const { classes } = this.props

    if (result !== null) {
      return <Redirect to={`/r/${result.name}`} />
    }

    return (
      <Paper className={classes.root}>
        <FormGroup>
          <h1>Create a new Subribbit</h1>

          <TextField
            autoFocus
            margin='dense'
            id='name'
            label='Name'
            fullWidth
            required
            onChange={this.handleNameChange}
            value={name}
          />

          <Button to='/' component={Link} color='secondary'>
            Cancel
          </Button>
          <Button onClick={this.handleSubmit} color='primary'>
            Create
          </Button>
        </FormGroup>
      </Paper>
    )
  }
}
CreateSubribbit.contextType = Context

export default withStyles(styles)(CreateSubribbit)
