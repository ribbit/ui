import React from 'react'
import Avatar from '@material-ui/core/Avatar'

interface Props {
  subribbit: string
}

export default function SubribbitAvatar({ subribbit } : Props) {
  return (
    <Avatar aria-label='recipe'>
      {subribbit.charAt(0)}
    </Avatar>
  )
}
