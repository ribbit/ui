import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'

import { Context } from '../Context'
import SubribbitSelector from './SubribbitSelector'
import SubribbitsClient, { Subribbit, DEFAULT_SUBRIBBIT } from '../api/subribbitsClient'

interface Props {
  subribbit: Subribbit
}

interface State {
  subscribing: boolean,
  loading: boolean,
  subscribed: boolean
}

export default class SubribbitTools extends Component<Props, State> {
  state = { subscribing: false, loading: false, subscribed: false }

  async componentDidMount () {
    this.reload()
  }

  componentDidUpdate (prevProps: Props) {
    if (this.props.subribbit !== prevProps.subribbit) {
      this.reload()
    }
  }

  async reload () {
    const client: SubribbitsClient = this.context.subribbitsClient
    const { subribbit } = this.props

    this.setState({ loading: true })
    const subscriptions = await client.getSubscriptions()
    const subscribed = subscriptions.indexOf(subribbit) !== -1
    this.setState({ subscribed, loading: false })
  }

  async handleSubscribe () {
    const subribbitsClient: SubribbitsClient = this.context.subribbitsClient
    const { subribbit } = this.props

    this.setState({ subscribing: true })
    await subribbitsClient.subscribe(subribbit!.name)
    this.setState({ subscribing: false, subscribed: true })
  }

  async handleUnsubscribe () {
    const subribbitsClient: SubribbitsClient = this.context.subribbitsClient
    const { subribbit } = this.props

    this.setState({ subscribing: true })
    await subribbitsClient.unsubscribe(subribbit!.name)
    this.setState({ subscribing: false, subscribed: false })
  }

  render () {
    const { subribbit } = this.props
    const { subscribing, loading, subscribed } = this.state
    const userId: string = this.context.user.getId()
    const isCreator = subribbit.creator === userId

    const handleSubscribe = this.handleSubscribe.bind(this)
    const handleUnsubscribe = this.handleUnsubscribe.bind(this)

    function SubscribeUnsubscribeButton () {
      if (subribbit === DEFAULT_SUBRIBBIT) {
        return null
      }

      if (subscribed && !isCreator) {
        return (
          <Button onClick={handleSubscribe} disabled={subscribing || loading}>
            Unsubscribe
            {subscribing || loading ? <CircularProgress /> : null}
          </Button>
        )
      }

      if (!subscribed && !isCreator) {
        return (
          <Button color='primary' onClick={handleUnsubscribe} disabled={subscribing || loading}>
            Subscribe
            {subscribing || loading ? <CircularProgress /> : null}
          </Button>
        )
      }

      return null
    }

    function CreatePostButton() {
      if (subribbit === DEFAULT_SUBRIBBIT) {
        return null
      }

      return (
        <div>
            <Button color='primary' to={`/r/${subribbit.name}/new`} component={Link}>
              Create Post
            </Button>
          </div>
      )
    }

    return (
      <Card>
        <CardContent>
          <div>
            <SubribbitSelector selected={subribbit} />
          </div>
          <div>
            <Button color='primary' to='/new' component={Link}>
              Create Subribbit
            </Button>
          </div>
          <CreatePostButton />
          <div>
            <SubscribeUnsubscribeButton />
          </div>
        </CardContent>
      </Card>
    )
  }
}
SubribbitTools.contextType = Context
