export default class Auth {
  private googleUser: any

  constructor(googleUser: any) {
    this.googleUser = googleUser
  }

  async getIdToken(): Promise<string> {
    const { googleUser } = this

    const expiresAt = new Date(googleUser.getAuthResponse().expires_at)
    if (expiresAt < new Date()) {
      await googleUser.reloadAuthResponse()
    }

    return googleUser.getAuthResponse().id_token
  }
}