import axios, { AxiosInstance } from 'axios'
import { apiHost } from '../config'
import Auth from './auth'

export interface User {
  id: string,
  name: string
  lastLogin: Date
}

export default class UsersClient {
  private client: AxiosInstance

  constructor (auth: Auth) {
    this.client = axios.create({
      baseURL: apiHost + '/users/v1/'
    })
    this.client.interceptors.request.use(async function (config) {
      config.headers.Authorization = `Bearer ${await auth.getIdToken()}`
      return config
    })
  }

  async onLogin (googleUser: any): Promise<void> {
    return this.client.post(`/account`, { display_name: googleUser.getBasicProfile().getName() })
  }

  async get (id: string): Promise<User> {
    const data = (await this.client.get(`/u/${id}`)).data

    return {
      id: data.id,
      name: data.display_name,
      lastLogin: data.last_login
    }
  }
}
