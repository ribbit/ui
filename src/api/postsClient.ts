import axios, { AxiosInstance, AxiosResponse } from 'axios'
import { apiHost } from '../config'
import Auth from './auth'

export interface Post {
  id: string
  title: string
  type: PostType
  value: string
  author: string
  subribbit: string
  posted: Date
}

export enum PostType {
  text = 'text',
  image = 'image',
  youtube = 'youtube',
  link = 'link',
  video = 'video'
}

export default class PostsClient {
  private client: AxiosInstance

  constructor (auth: Auth) {
    this.client = axios.create({
      baseURL: apiHost + '/posts/v1/'
    })
    this.client.interceptors.request.use(async function (config) {
      config.headers.Authorization = `Bearer ${await auth.getIdToken()}`
      return config
    })
  }

  async list (subribbit: string): Promise<AxiosResponse<Post[]>> {
    return this.client.get(`/r/${subribbit}/p`)
  }

  async create (subribbit: string, title: string, type: PostType, value: string): Promise<AxiosResponse<Post>> {
    return this.client.post(`/r/${subribbit}/p`, { title, type, value })
  }

  async delete (post: Post): Promise<AxiosResponse<Post>> {
    return this.client.delete(`/r/${post.subribbit}/p/${post.id}`)
  }
}
