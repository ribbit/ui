import axios, { AxiosInstance } from 'axios'
import { apiHost } from '../config'
import Auth from './auth'

export interface Subribbit {
  name: string
  creator: string
  private: boolean
}

export const DEFAULT_SUBRIBBIT: Subribbit = {
  name: 'all',
  creator: 'ribbit',
  private: false
}

export default class SubribbitsClient {

  private client: AxiosInstance

  constructor (auth: Auth) {
    this.client = axios.create({
      baseURL: apiHost + '/subribbits/v1/'
    })
    this.client.interceptors.request.use(async function (config) {
      config.headers.Authorization = `Bearer ${await auth.getIdToken()}`
      return config
    })
  }

  async getSubribbit(name: string): Promise<Subribbit | null> {
    const response = await this.client.get(`/r`, { params: { name }})
    return response.data[0]
  }

  async create (name: string): Promise<Subribbit> {
    const response = await this.client.post('/r', { name })
    return response.data
  }

  async subscribe (name: string): Promise<void> {
    return this.client.post(`/s/${name}`)
  }

  async subscribe_user (subribbit: string, userId: string): Promise<void> {
    return this.client.post(`/r/${subribbit}/s`, { user_id: userId })
  }

  async unsubscribe (name: string): Promise<void> {
    return this.client.delete(`/s/${name}`)
  }

  async unsubscriber_user (subribbit: string, userId: string): Promise<void> {
    return this.client.delete(`/r/${subribbit}/s`, { data: { user_id: userId } })
  }

  async getSubscribers(subribbit: string): Promise<string[]> {
    const response = await this.client.get(`/r/{subribbit}/s`)
    return response.data
  }

  async getSubscriptions (): Promise<Subribbit[]> {
    const response = await this.client.get(`/s`)
    return response.data
  }

  async delete (subribbit: string): Promise<Subribbit> {
    const response = await this.client.delete(`/r/${subribbit}`)
    return response.data
  }

  async makePrivate (subribbit: string): Promise<Subribbit> {
    const response = await this.client.patch(`/r/${subribbit}`, { private: true })
    return response.data
  }

  async makePublic (subribbit: string): Promise<Subribbit> {
    const response = await this.client.patch(`/r/${subribbit}`, { private: false })
    return response.data
  }
}
