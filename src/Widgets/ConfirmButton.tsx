import React, { Component } from 'react'

import Button from '@material-ui/core/Button'
import { CircularProgress } from '@material-ui/core'

interface Props {
  confirmMessage?: string
  color?: "inherit" | "default" | "primary" | "secondary"
  onClick: Function
  disabled?: boolean
}

interface State {
  loading: boolean
}

const defaultConfirmMessage = 'Are you sure?'

export default class ConfirmButton extends Component<Props, State> {
  public state = { loading: false }

  async handleClick () {
    const { confirmMessage, onClick } = this.props
    if (window.confirm(confirmMessage || defaultConfirmMessage)) {
      this.setState({ loading: true })
      await onClick()
      this.setState({ loading: false})
    }
  }

  render () {
    const { color, children, disabled } = this.props
    const { loading } = this.state

    return (
      <div>
        <Button
          color={color || 'inherit'}
          onClick={this.handleClick.bind(this)}
          disabled={disabled || loading || false}
        >
          {children}
        </Button>
        {loading ? <CircularProgress /> : null}
      </div>
    )
  }
}
