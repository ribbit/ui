import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import Header from './Header/Header'
import PostsList from './Posts/PostsList'
import CreatePost from './Posts/CreatePost'
import CreateSubribbit from './Subribbits/CreateSubribbit'
import SubribbitLoader from './Subribbits/SubribbitLoader'

import { Context, Provider } from './Context'
import { DEFAULT_SUBRIBBIT } from './api/subribbitsClient'

class Main extends Component {
  render () {
    const { loggedIn } = this.context

    return (
      <Router>
        <Header />

        {loggedIn ? (
          <div>
            <Route path='/' exact component={ListAllPosts} />
            <Route path='/new' exact component={CreateSubribbit} />
            <Route path='/r/:subribbit/' exact component={ListPostsByRoute} />
            <Route path='/r/:subribbit/new' exact component={CreatePostByRoute} />
          </div>
        ) : null}
      </Router>
    )
  }
}
Main.contextType = Context

export default function App () {
  return (
    <Provider>
      <Main />
    </Provider>
  )
}

function ListAllPosts () {
  return <PostsList subribbit={DEFAULT_SUBRIBBIT} />
}

function ListPostsByRoute (props: any) {
  const subribbit: string = props.match.params.subribbit
  return <SubribbitLoader name={subribbit} />
}

function CreatePostByRoute (props: any) {
  const subribbit: string = props.match.params.subribbit
  return <CreatePost subribbitName={subribbit} />
}